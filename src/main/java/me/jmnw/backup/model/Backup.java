/*
 § Copyright (c) 2015  Joel Messerli
 §
 § This program is free software: you can redistribute it and/or modify
 § it under the terms of the GNU General Public License as published by
 § the Free Software Foundation, either version 3 of the License, or
 § (at your option) any later version.
 §
 § This program is distributed in the hope that it will be useful,
 § but WITHOUT ANY WARRANTY; without even the implied warranty of
 § MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 § GNU General Public License for more details.
 §
 § You should have received a copy of the GNU General Public License
 § along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jmnw.backup.model;

import me.jmnw.backup.io.ListFilesVisitor;
import me.jmnw.backup.util.hash.FileHasher;
import me.jmnw.backup.util.hash.SHA512FileHasher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

import static me.jmnw.backup.model.BackupType.FULL;

public class Backup {
    private BackupType type;
    private LocalDateTime time;
    private HashMap<Path, String> fileHashes;
    private Backup parent;

    private static FileHasher fileHasher = new SHA512FileHasher();

    private Backup() {
    }

    public static Backup newFullBackup(Path folder) {
        if (!Files.isDirectory(folder)) throw new IllegalArgumentException("folder must be a directory");
        ListFilesVisitor visitor = new ListFilesVisitor();

        try {
            Files.walkFileTree(folder, visitor);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return newFullBackup(visitor.getPaths());
    }

    public static Backup newFullBackup(ArrayList<Path> files) {
        Backup backup = new Backup();
        backup.setType(FULL);
        backup.setTime(LocalDateTime.now());

        HashMap<Path, String> hashes = new HashMap<>();
        for (Path file : files) hashes.put(file, fileHasher.getHash(file));
        backup.setFileHashes(hashes);

        return backup;
    }

    // TODO
    public static Backup newIncrementalBackup(Backup parent) {
        HashMap<Path, String> fileHashes = parent.getAllFileHashes();

        return null;
    }

    /**
     * Used to get all files from an incremental backup state, even if they were not backed up in the current version
     *
     * @return A map with all the files and hashes in it
     */
    public HashMap<Path, String> getAllFileHashes() {
        if (type == FULL) return fileHashes;
        // TODO
        return null;
    }

    //region Getters and setters

    public BackupType getType() {
        return type;
    }

    public void setType(BackupType type) {
        this.type = type;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public HashMap<Path, String> getFileHashes() {
        return fileHashes;
    }

    public void setFileHashes(HashMap<Path, String> fileHashes) {
        this.fileHashes = fileHashes;
    }

    public Backup getParent() {
        return parent;
    }

    public void setParent(Backup parent) {
        this.parent = parent;
    }
    //endregion
}
