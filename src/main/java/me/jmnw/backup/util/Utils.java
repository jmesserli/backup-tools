/*
 § Copyright (c) 2015  Joel Messerli
 §
 § This program is free software: you can redistribute it and/or modify
 § it under the terms of the GNU General Public License as published by
 § the Free Software Foundation, either version 3 of the License, or
 § (at your option) any later version.
 §
 § This program is distributed in the hope that it will be useful,
 § but WITHOUT ANY WARRANTY; without even the implied warranty of
 § MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 § GNU General Public License for more details.
 §
 § You should have received a copy of the GNU General Public License
 § along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jmnw.backup.util;

public class Utils {

    private final static char[] hexValues = "0123456789abcdef".toCharArray();

    /* 20.06.15, from: http://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java */
    public static String byteArrayToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2]; // byte: 8 bit, 1 hex char: 4 bit

        for (int i = 0; i < bytes.length; i++) {
            int intVal = bytes[i] & 0xFF;
            hexChars[2 * i] = hexValues[intVal >>> 4]; // unsigned right shift by 4 bits
            hexChars[2 * i + 1] = hexValues[intVal & 0xF]; // intVal & 0b1111 (last 4 bits)
        }

        return new String(hexChars);
    }
}
