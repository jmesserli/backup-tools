/*
 § Copyright (c) 2015  Joel Messerli
 §
 § This program is free software: you can redistribute it and/or modify
 § it under the terms of the GNU General Public License as published by
 § the Free Software Foundation, either version 3 of the License, or
 § (at your option) any later version.
 §
 § This program is distributed in the hope that it will be useful,
 § but WITHOUT ANY WARRANTY; without even the implied warranty of
 § MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 § GNU General Public License for more details.
 §
 § You should have received a copy of the GNU General Public License
 § along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jmnw.backup.util.hash;

import me.jmnw.backup.util.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.MessageDigest;

public class GenericFileHasher implements FileHasher {

    private MessageDigest digest;

    @Override
    public String getHash(Path file) {
        try (InputStream is = Files.newInputStream(file)) {
            DigestInputStream dis = new DigestInputStream(is, digest);

            int read;
            do {
                read = dis.read();
            } while (read != -1);

            dis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] digest = this.digest.digest();
        return Utils.byteArrayToHex(digest);
    }

    public MessageDigest getDigest() {
        return digest;
    }

    public void setDigest(MessageDigest digest) {
        this.digest = digest;
    }
}
